# stakefish-takehome

Code for the DS Engineer stakefish take home questions

## Installation
See requirements.txt for the list of python libraries I use.

## Usage
The stakefish.ipynb shows an interactive exploration of the data, which includes most of my thoughts during this project. You can also use the stakefish.py from the command line.

usage: stakefish.py [-h] [--pretty-plots PRETTY_PLOTS] [--within-time WITHIN_TIME] [--use-saved-data USE_SAVED_DATA]

optional arguments:

  **--pretty-plots PRETTY_PLOTS default=True**

  Set to False if you do not want to display plots.

  **--within-time WITHIN_TIME default=86400**

  Time in seconds, use to return "What is the probability a long block will occur in t seconds?"

  **--use-saved-data USE_SAVED_DATA default=True**
    
  Note that you will need to setup BigQuery locally if you want to query live data: https://cloud.google.com/bigquery/docs/reference/libraries#setting_up_authentication