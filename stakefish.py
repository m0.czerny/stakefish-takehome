import os
import argparse

import numpy as np
import datetime as dt

from google.cloud import bigquery
from scipy import stats


def collect_data(from_saved: bool=True, fpath: str='') -> list[tuple]:
    if from_saved:
        data = []
        if fpath == '':
            fpath = os.path.join(os.getcwd(), 'data', 'blockdata.csv')
        with open(fpath, 'r') as infile:
            for line in infile.readlines():
                block_num, block_time = line.strip('\n').split(',')
                block_num = int(block_num)
                block_time = dt.datetime.fromisoformat(block_time) # 2009-01-10 04:45:58+00:00
                data.append((block_num, block_time))
            
        return data
    else:
        """
        Note that you will need to setup big query locally if you want to
        query live data: 
        https://cloud.google.com/bigquery/docs/reference/libraries#setting_up_authentication
        """
        client = bigquery.Client()
        
        query = """
            SELECT number, timestamp
            FROM `bigquery-public-data.crypto_bitcoin.blocks`
        """
        
        query_job = client.query(query)
        query_results = query_job.result(timeout=30)
        data = [row.values() for row in query_results]
        
        return data
    
    
def calc_time_between_long_blocks(block_data: list[tuple], threshold: int=2*60*60) -> list[float]:
    """
    We want to identify the time difference between "long" blocks. Long blocks
    in this case refers to blocks that took more than 2 hours to mine. Our convention
    is to include the mining time into this time difference.
    For example, if block 0 took 2 hours, block 1 took 10 minutes, block 2 took 1 hour,
    and block 3 took 2.5 hours, then the difference between long block 0 and long block 3
    is 10 min + 1 hour + 2.5 hours.

    Parameters
    ----------
    block_data : list[tuple]
        Tuple format should be (block_num: int, timestamp: datetime).
    threshold : int:
        Threshold in seconds of how we define a long block (default is 2 hours)
    Returns
    -------
    list[float]
        Return a list of all the calculated times between long blocks.
    """
    block_data = sorted(block_data, key=lambda tup: tup[0]) # sort on block num ascending
    tstamps = [z[1] for z in block_data]
    inter_block_times = []
    last_ts = tstamps[0]
    
    for i in range(1, len(tstamps)):
        diff = (tstamps[i] - tstamps[i-1]).total_seconds()
        if diff > threshold:
            inter_time = (tstamps[i] - last_ts).total_seconds()
            inter_block_times.append(inter_time)
            last_ts = tstamps[i]
    return inter_block_times
    

def fit_weibull_dist(data: list[float], loc: float=4.0) -> tuple:
    params = stats.exponweib.fit(data, loc=loc)
    return params
    

def prob_lblock_within_t(time: int, data: list[float], wb_params: tuple) -> float:
    x = np.linspace(min(data), max(data), 100)
    surv_func = stats.exponweib.sf(x, *wb_params)
    
    log_time = np.log10(time)
    
    index = np.where(x < log_time)[0][-1]
    return round(100 * (1 - surv_func[index]), 2)


def plot_dist_and_s_func(data: list[float], wb_params: tuple) -> None:
    plot_x = np.linspace(min(data), max(data), 100)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    _ = ax1.hist(data, bins=20, density=True)
    ax1.plot(plot_x, stats.exponweib.pdf(plot_x, *wb_params))
    ax1.set_title('Time Between Long Blocks Histogram')
    ax1.set_xlabel('Log10 Time (s)')
    
    ax2 = fig.add_subplot(212)
    sf = stats.exponweib.sf(plot_x, *wb_params)
    ax2.plot(plot_x, sf)
    ax2.set_title('Survival Function')
    ax2.set_ylabel('Probability')
    ax2.set_xlabel('Log10 Time (s)')
    
    plt.show()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--pretty-plots', type=bool, default=True,
                        help='Set to False if you do not want to display plots.')
    parser.add_argument('--within-time', type=int, default=24*60*60,
                        help="""Time in seconds, use to return 
                        "What is the probability a long block will occur in t seconds?"
                        """)
    parser.add_argument('--use-saved-data', type=bool, default=True,
                        help="""
                        Note that you will need to setup BigQuery locally if you want to
                        query live data: 
                        https://cloud.google.com/bigquery/docs/reference/libraries#setting_up_authentication
                        """)
                        
    args = parser.parse_args()
    
    block_data = collect_data(args.use_saved_data)
    inter_event_times = calc_time_between_long_blocks(block_data)
    log_times = np.log10(inter_event_times)
    
    fitted_params = stats.exponweib.fit(log_times, loc=4)
        
    log_median_time = stats.exponweib.median(*fitted_params)
    median_time = pow(10, log_median_time) / (60 * 60)
    
    lblock_prob = prob_lblock_within_t(args.within_time, log_times, fitted_params)
    
    print(f'Median time to long block: {median_time} hrs.')
    print(f'Probability long block will occur within {args.within_time} seconds: {lblock_prob}%')
    print(f'Total number of long blocks: {len(log_times)}.')
    
    if args.pretty_plots:
        import matplotlib.pyplot as plt
        plot_dist_and_s_func(log_times, fitted_params)
